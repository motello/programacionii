/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author motello
 */
public class Alumno extends Persona implements Becado {

    String carrera;

    public Alumno(String nombre, String carrera) {
        super(nombre);
        this.carrera = carrera;
    }

    

    @Override
    public String getDescripcion() {
        return "Esta es la descripcion de alumno";

    }

    @Override
    public void tiempoBeca(int tiempo) {
        
    }

    @Override
    public int obtenerTiempoBeca() {
        return 5;
    }

    @Override
    public int obtenerCosto() {
        return 5000;
    }

}
