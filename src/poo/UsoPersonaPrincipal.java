/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author motello
 */
public class UsoPersonaPrincipal {
    
    public static void main(String[] args) {
    
        /*
        Empleado emp1 = new Empleado("Jhonny", 2000, 2018, 1, 1, 1);
       
        */
        
         Alumno alu1 = new Alumno("Juanito", "Ingenieria");
       
        
        // En la siguiente linea estamos creando un arreglo de tipo Persona, NO es un objeto tipo persona.
        // Una clase abstracta no se puede instanciar
       
        //forma de agregar objetos en un array
        // inicio --> 
        //Persona[] lasPersonas = new Persona[2];  
        //lasPersonas[0] = new Empleadito("Pedro", 2000, 2018, 1, 1, 1); // Instanciamos un objeto Empleadito
        //lasPersonas[1] =new Alumno("Juanito", "Ingenieria"); //Instanciamos un objeto Alumno
        // <-- fin 
        
        
        // Este es un for mejorado
        //for(Persona p: lasPersonas){
          //  System.out.println("Nombre: " + p.getNombre() + " y " +p.getDescripcion());// cada vez que el for pasa por el metdodo getDescripcion sabe que metodo utilizar ya que es abstracto
        //}
        
        
        /*
        System.out.println(emp1.getDescpricion());
        System.out.println(alu1.getDescpricion());
        */
    }

   

    
    
}
