/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author motello
 */
public abstract class Persona {
    
    private String nombre;
    
    public Persona(String nombre){
    
        this.nombre = nombre;
        
    }
    
    public String getNombre(){
        
        return nombre;
    }
    
    //Este es un metodo abstracto
    //El cual se utiliza para que otras clases lo implementen y lo utilicen de la forma que se requiera
    //Solamente se declara el metodo, es decir no se le hace cuerpo entre llaves.
    public abstract String getDescripcion();
    
    
}
