
package poo;


public class Jefe extends Empleadito{
    
    
    
    private double incentivo;

    public Jefe(String nombre, double sueldo, int anio, int mes, int dia, int id){
    
        super(nombre, sueldo, anio, mes, dia, id);
        
    }
    
    
    public void establecerSueldo(double b){
    incentivo = b;
    
    }

    public double getIncentivo() {
        return incentivo;
    }

    public void setIncentivo(double incentivo) {
        this.incentivo = incentivo;
    }
    
    
    @Override
    public double getSueldo(){
       double sueldoJefe = super.getSueldo();
    return sueldoJefe+incentivo;
     }
    
    
    
 
}
