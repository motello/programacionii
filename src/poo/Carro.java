package poo;

import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author motello
 */
public class Carro {
    
    private String color;
    private int largo;
    private int ancho;
    private int peso;
    private int ruedas;
    private String marca;
    private boolean asientosCuero, turbo;
    private Date j;

    public Carro() {
        
        color="Rojo";
        largo=4000;
        ancho=5000;
        peso=2000;
        ruedas=4;
        marca="Honda";
 
    }
    
    public String getColor(){

        return "EL COLOR ES " +color;
        
    }
    
    public void setColor(String colorCarro){
    
        color = colorCarro;
    
    }
    
    public void setAsientosCuero(String asientosCuero){
        if (asientosCuero.equalsIgnoreCase("si")) {
            this.asientosCuero = true;
        }
        else{this.asientosCuero=false;}
    }
    
    public String getAsientosCuero(){
    
        if (asientosCuero) {
            return "Si tiene";
        } else {
            return "No tiene";
        }
        
    }
    
    
    
    
    
}
