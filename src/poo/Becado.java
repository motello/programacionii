
package poo;

public interface Becado {
    
    public void tiempoBeca(int tiempo);
    public int obtenerTiempoBeca();
    public int obtenerCosto();    
}
