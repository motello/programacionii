/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author motello
 */
public class Empleadito extends Persona {

    public Empleadito(String nombre, double sueldo, int anio, int mes, int dia, int id) {
        super(nombre); 
        this.sueldo = sueldo;
        this.id = id; //NUEVA INICIALIZACION //1o.
        //id=1; //delete ant //2o.
        //id=idsiuiente; //3o.
        //idSiguiente++;

        GregorianCalendar calendario = new GregorianCalendar(anio, mes - 1, dia);

        fechaIngreso = calendario.getTime();

    }

    public Empleadito(String nombre) { //sobrecarga de constructor

        this(nombre, 34, 2018, 8, 4, 5);

    }

    private double sueldo;
    private Date fechaIngreso;
    private int id;

    public double getSueldo() {
        return sueldo;
    }

    public void subirSueldo(double porcentaje) {
        double aumento = sueldo * porcentaje / 100;
        sueldo += aumento;

    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public int getId() {
        return id;
    }

    
    //este es un metodo heredado de la super clase Persona
    //cuando heredamos de una clase abstracta tenemos que implementar el metodo abstracto de esa super clase si o si
    // e implementarlo de la manera que nos sea necesario
    @Override
    public String getDescripcion() {

        return "Esta es la descripcion de un Empleadito, y tambien una manera distinta de usar este metodo"
                + "que se esta heredando de la clase abstracta";
    }

}
